<?php
    function fibonacci($n){
        if ($n < 2){
            return $n;
        }
        else{
            return fibonacci($n - 1) + fibonacci($n - 2);
        }
    }

$numero = 10;
$resultado = fibonacci($numero);

echo "Elemento nº $numero de fibonacci: $resultado\n";
?>
