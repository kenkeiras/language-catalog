function fibonacci(n){
    if (n < 2){
        return n;
    }
    else{
        return fibonacci(n - 1) + fibonacci(n - 2);
    }
}

var numero = 10;
var resultado = fibonacci(numero);

// Esto cambia segun el entorno, para webs
//document.write("Elemento nº "+ numero +" de fibonacci: "+resultado+"<br />");

// Para nodejs
process.stdout.write("Elemento nº "+ numero +" de fibonacci: "+resultado+"\n");

