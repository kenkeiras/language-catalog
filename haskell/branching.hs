-- Función
fizzbuzz :: () => Int -> String

fizzbuzz numero 
             | (mod numero 3 > 0) && (mod numero 5 > 0)   = show numero
             | (mod numero 3 == 0) && (mod numero 5 == 0) = "FizzBuzz"
             | mod numero 3 == 0                          = "Fizz"
             | otherwise                                  = "Buzz"

main = do 
    mapM_ (putStrLn . fizzbuzz) [1..100] -- si no te gusta mapM_ usar forM (lo mismo pero coge los argumentos alrevez)

