import Control.Monad

type MyType = [String] -- simplente MyType equivale a [String] no crea un nuevo tipo simplemente le da otro nombre

-- un documento podría ser una revista, o un libro
data Documentos = Revistas String Int -- nombre, número
                                | Libro   String    -- nombre
                                deriving (Eq, Show) -- se deriva de las clases(Eq (permite usar (==) (/=))y show(lo pasa a string))
-- Tipos de datos principales(se pueden crear mas tipos a partir data)
-- En haskell no es necesario poner a :: Int, pero algunas situaciones lo requieren. En estas no, pero no pasa nada

a :: Int
a = 1
-- No hay variables en haskell (No se puede hacer a+=1)
b :: Bool
b = True
c :: [Char] -- [Char] es equivalente String
c = "Hola :P"

main :: IO ()
main = do
  x <- getContents   -- obtiene una linea
  putStrLn x         -- imprime una linea
  return ()          -- no es igual a un return de los lenguajes imperativos, introduce el valor en un tipo en este caso IO () 
