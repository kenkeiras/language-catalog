import Text.Printf

fibonacci 0 = 0
fibonacci 1 = 1
fibonacci n = fibonacci(n - 1) + fibonacci (n - 2)

main = do
    let numero = 10
    let resultado = fibonacci numero
    printf "Elemento nº %i de fibonacci: %i\n" (numero::Int) (resultado::Int)
