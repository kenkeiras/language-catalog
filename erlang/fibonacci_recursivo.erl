-module(fibonacci_recursivo).
-export([start/0, fibonacci/1]).

fibonacci(0) -> 0;
fibonacci(1) -> 1;
fibonacci(N) -> fibonacci(N - 1) + fibonacci(N - 2).


start() -> 
     Numero = 10,
     Resultado = fibonacci(Numero),
     io:format("Elemento nº ~p de fibonacci: ~p~n", [Numero, Resultado]).
