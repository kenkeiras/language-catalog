using System;
 
class Program{

    static int fibonacci(int n){
        if (n < 2){
            return n;
        }
        else{
            return fibonacci(n - 1) + fibonacci(n - 2);
        }
    }

    static void Main(){
        int numero = 10;
        int resultado = fibonacci(numero);
        Console.Write("Elemento nº ");
        Console.Write(numero);
        Console.Write(" de fibonacci: ");
        Console.WriteLine(resultado);
    }
}
