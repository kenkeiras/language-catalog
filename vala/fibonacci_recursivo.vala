int fibonacci(int n){
    if (n < 2){
        return n;
    }
    else{
        return fibonacci(n - 2) + fibonacci(n - 1);
    }
}

void main(){
     int numero = 10;
     int resultado = fibonacci(numero);

     stdout.printf("Elemento nº %i de fibonacci: %i\n",numero, resultado);
}
