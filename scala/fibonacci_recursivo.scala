def fibonacci(n: Int): Int = if (n < 2) n
                             else fibonacci(n - 1) + fibonacci(n - 2)

val numero = 10
val resultado = fibonacci(numero)

Console.printf("Elemento nº %s de fibonacci: %s\n", numero, resultado)
