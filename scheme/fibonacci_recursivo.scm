(define (fibonacci n) 
 (if (< n 2)
  n
  (+ (fibonacci (- n 2)) (fibonacci (- n 1)))
 )
)

(define numero 10)
(define resultado (fibonacci numero))

(display "Elemento nº"    )
(display numero           )
(display " de fibonacci: ")
(display resultado        )
(newline                  )
