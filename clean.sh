#!/usr/bin/env sh
# Compila todos los lenguajes

for i in `find -maxdepth 1 -type d|cut -d/ -f2|grep -v '\\.'`;do
    make -C $i clean
    if [ $? -ne 0 ];then
        echo "Error cleaning $i"
        break
    fi
done
