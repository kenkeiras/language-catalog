program fibonacci_recursivo;

function fibonacci(n: integer): integer;
    var r: integer;
begin
    if (n < 2) then
        r := n
    else
        r := fibonacci(n - 1) + fibonacci(n - 2);
    fibonacci := r;
end;

var
    numero, resultado: integer;

begin
    numero := 10;
    resultado := fibonacci(numero);
    writeln('Elemento nº ', numero, ' de fibonacci: ', resultado);
end.
