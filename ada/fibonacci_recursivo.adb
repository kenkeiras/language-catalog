with Ada.Text_IO, Ada.Integer_Text_IO;
use Ada.Text_IO, Ada.Integer_Text_IO;
 
procedure Fibonacci_Recursivo is
    function Fibonacci(n : in Integer) return Integer is
    begin
            if n < 2 then
                return n;
            else
                return Fibonacci(n - 1) + Fibonacci(n - 2);
            end if;
    end Fibonacci;


    Numero : Integer := 10;
    Resultado : Integer := Fibonacci(Numero);

begin

    Put_Line(Item => "Elemento nº" & Integer'Image (Numero) &
                     " de fibonacci:" & Integer'Image(Resultado));
 
end Fibonacci_Recursivo;
