[bits 32]
section .data
    numero: dd 10
    format_string: db 'Elemento nº %i de fibonacci: %i', 10, 0

section .text

global main
extern printf

; pasaremos el parametro por eax
fibonacci:
    ; solo hay cambios si eax es menor que 2
    cmp eax, 2
    jl .fibonacci_done

    ; Si n >= 2
    ;  a = fibonacci(n - 1)
    push dword eax ; guardamos n
    dec eax        ; le restamos 1
    call fibonacci ; llamamos a fibonacci
    pop ebx        ; recuperamos el original en ebx

    ;  b = fibonacci(n - 2)
    push eax       ; guardamos fibonacci(n - 1)
    mov eax, ebx   ; pasamos el original a eax
    sub eax, 2     ; le restamos 2
    call fibonacci ; llamamos a fibonacci
    pop ebx        ; recuperamos el original

    ; a + b
    add eax, ebx   ; y los sumamos

.fibonacci_done:
    ret


main:
    mov eax, [numero]
    call fibonacci

    ; a C los parametros se le pasan al reves
    push dword eax            ; resultado
    push dword [numero]       ; numero
    push dword format_string  ; "Elemento nº %i de fibonacci: %i\n"
    call printf

; exit_syscall, se llama al kernel para salir
    mov eax, 1
    mov ebx, 0
    int 0x80


