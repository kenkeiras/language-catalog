#import <stdio.h>

int fibonacci(int n){
    if (n < 2){
        return n;
    }
    else{
        return fibonacci(n - 1) + fibonacci(n - 2);
    }
}

int main(){
    int numero = 10;
    int resultado = fibonacci(numero);

    printf("Elemento nº %i de fibonacci: %i\n", numero, resultado);

    return 0;
}

