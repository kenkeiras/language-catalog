fibonacci <- function(n){
    return (ifelse (n < 2, n, fibonacci(n - 2) + fibonacci(n - 1)))
}

numero <- 10
resultado <- fibonacci(numero)

s <- sprintf("Elemento nº %i de fibonacci: %i", numero, resultado)
print(s)
