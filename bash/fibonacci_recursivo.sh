fibonacci(){
    if [ $1 -lt 2 ];then
        echo $1;
    else
        echo $(( `fibonacci $(( $1 - 1 ))` + `fibonacci $(( $1 - 2 ))` ));
    fi
}

numero=10
resultado=`fibonacci $numero`

echo "Numero $numero de fibonacci: $resultado"
