int fibonacci(int n) {
    if (n < 2){
        return n;
    }
    else{
        return fibonacci(n - 1) + fibonacci(n - 2);
    }
}

main() {
  int numero = 10;
  int resultado = fibonacci(numero);
  // ó print('Elemento nº ${numero} de fibonacci: ${fibonacci(numero)');
  print('Elemento nº ${numero} de fibonacci: ${resultado}');
}
