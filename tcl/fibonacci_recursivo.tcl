proc fibonacci n {
    if { $n < 2 } {
        return $n
    } else {
        return [expr [fibonacci [expr $n - 1]] + [fibonacci [expr $n - 2]]]
    }
}

set numero 10
set resultado [fibonacci $numero]
puts [format "Elemento nº %i de fibonacci: %i" $numero $resultado]
