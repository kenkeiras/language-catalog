function fibonacci(n)
    if n < 2 then
        return n
    else
        return fibonacci(n - 1) + fibonacci(n - 2)
    end
end

numero = 10
resultado = fibonacci(numero)

io.write(string.format("Elemento nº %i de fibonacci: %i\n", numero, resultado))
