class fibonacci_recursivo{
    static int fibonacci(int n){
        if (n < 2){
            return n;
        }
        else{
            return fibonacci(n - 1) + fibonacci(n - 2);
        }
    }

    public static void main(String argv[]){
        int numero = 10;
        int resultado = fibonacci(numero);

        System.out.println("Elemento nº "+ numero +" de fibonacci: " + resultado);

    }
}
