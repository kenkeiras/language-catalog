package main

import "fmt"

func fibonacci(n int)(r int){
    if (n < 2){
        r = n
    }else{
        r = fibonacci(n - 1) + fibonacci(n - 2)
    }
    return 
}

func main() {
    numero := 10
    resultado := fibonacci(numero)
    fmt.Printf("Elemento nº %v de fibonacci: %v\n", numero, resultado)
}

