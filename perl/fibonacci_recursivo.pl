sub fibonacci{
    my $n = $_[0];
    if ($n < 2){
        return $n;
    }
    else{
        return fibonacci($n - 1) + fibonacci($n - 2);
    }
}

my $numero = 10;
my $resultado = fibonacci($numero);

print "Elemento nº $numero de fibonacci: $resultado\n";
