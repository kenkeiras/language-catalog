(defun fibonacci (n)
  (if (< n 2)
      n
      (+ (fibonacci (- n 1)) (fibonacci (- n 2)))))

(defvar numero 10)
(defvar resultado (fibonacci numero))
(format t "Numero ~D de fibonacci: ~D~%" numero resultado)
