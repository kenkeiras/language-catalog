def fibonacci(n: int): int
	if n < 2 do
		return n
	else
		return fibonacci(n - 1) + fibonacci(n - 2)

init
	var numero = 10
	var resultado = fibonacci(numero)
	print "Elemento nº %i de fibonacci: %i", numero, resultado
