USING: kernel math formatting ;
IN: fibonacci_recursivo

: fibonacci ( int -- int ) dup 2 < [ [ 1 - fibonacci ] [ 2 - fibonacci ] bi + ] unless ;

10
dup
fibonacci

"Elemento nº %d de fibonacci: %d" printf
