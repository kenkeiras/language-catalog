def fibonacci(n):
    if n < 2:
        return n
    else:
        return fibonacci(n - 1) + fibonacci(n - 2)


numero = 10 
resultado = fibonacci(numero)

print ("Elemento numero", numero, "de fibonacci:", resultado)
