let rec fibonacci n = if n < 2 then n
                      else fibonacci (n - 1) + fibonacci(n - 2)
in
let numero = 10 in
let resultado = fibonacci numero in
print_string "Elemento numero "; print_int numero;
print_string " de fibonacci: ";  print_int resultado; print_newline();;
