! Escrito para Fortran 95
PROGRAM fibonacci_recursivo
    IMPLICIT NONE
    INTEGER :: numero = 10, resultado
    resultado = fibonacci(numero)
    
    PRINT *, "Elemento nº ", numero, " de fibonacci: ", resultado

CONTAINS

RECURSIVE FUNCTION fibonacci(n) RESULT (r)
    INTEGER :: n, r

    IF (n < 2) THEN
        r = n
    ELSE
        r = fibonacci(n - 1) + fibonacci(n - 2)
    END IF
END FUNCTION fibonacci

END PROGRAM fibonacci_recursivo

