#!/usr/bin/env sh
# Compila todos los lenguajes

for i in `find -maxdepth 1 -type d|cut -d/ -f2|grep -v '\\.'`;do
    echo $i
    make -C $i build
    if [ $? -ne 0 ];then
        echo "Error building $i"
        break
    fi
done
